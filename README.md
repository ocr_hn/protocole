# Protocole standard d'océrisation (OCR) de documents dactylographiés grâce aux outils d'Huma-Num

Ce document présente un protocole standard à suivre pour l'océrisation de documents non-manuscrits. Il détaille les bonnes pratiques en termes de numérisation de documents papier et d'utilisation des outils d'OCR mis à disposition par l'IR* Huma-Num.

## I - Numérisation du document

La première étape dans le processus d'océrisation d'un document est sa numérisation. Il s'agit de capturer sous la forme d'une image numérique le ou les documents papier. 

le "document" peut être indifféremment considéré comme une "page" au format papier ou un ensemble de plusieurs d'entre elles. 

Deux solutions sont envisageables pour cette étape. Par ordre de préférence :

1. L'utilisation d'un scanner de documents

2. L'utilisation d'un appareil photo / smartphone

### A. Numérisation par scanner

- **Résolution :** Par défaut, choisissez une résolution de 400 DPI. Si la police d'écriture est petite (inferieure à 10 pt), vous pouvez augmenter à 600 DPI. Plus les DPI sont élevés, plus le processus d'OCR est efficace mais plus l'image est lourde et le traitement par OCR lent.

- **Format du fichier numérisé :** Choisissez un format de compression sans perte. Le format PDF est à  privilégier. Le format TIFF (.tif) est également un format de compression sans perte.

- **Luminosité :** La luminosité ne doit pas être trop élevée ou trop basse. Une luminosité de 50 % est généralement appropriée. 
  
  Si les documents contiennent des images à conserver, scannez en *couleur* (RGB). Sinon, vous pouvez scanner en *nuance de gris*. Le *noir et blanc* est à éviter.
  
  Assurez vous que le document est bien droit. Un document droit donnera de meilleurs résultats lors de l'OCR.

### B. Numérisation par photographie

- Utilisez un trépied pour la stabilité

- Utilisez un format d'image sans perte à la compression (TIFF)

- Désactivez le flash

## II - Océrisation du document numérisé

Huma-Num propose trois solutions d'océrisation de documents dans son outil de traitement disponible sur ShareDocs : Tesseract, AbbyCloud, AbbyServer.

[Tesseract](https://github.com/tesseract-ocr/tesseract) est un logiciel open source. La qualité de son OCR est inferieure aux deux solutions proposées par Abbyy (cloud et server). A la différence d'Abbyy, il n'y a pas de limite à son utilisation. Pour l'utiliser placez le ou les documents à océriser dans *hnTools_watchFolder/OCR/Tesseract/toTexte/[LANGUE DU DOCUMENT]* pour une sortie au format texte.

AbbyyCloud et AbbyyServer fonctionnent de la même manière. La différence étant qu'AbbyCloud ne supporte pas les fichiers de plus de 30Mo. Pour les utiliser placez le ou les documents à océriser dans *hnTools_watchFolder/OCR/abbyyServer [OU] abbyCloud/toTexte/[LANGUE DU DOCUMENT]* pour une sortie au format texte.

Vous serez notifié par mail lorsque l'extraction sera terminée. Une fois l'extraction terminée, un fichier *.txt* apparaitra au même emplacement.

Abbyy est à privilégier sur Tesseract mais chacune des deux versions (cloud et server) est limitée à 900 pages par utilisateur et par an.



[Documentation Huma-Num des outils d'océrisation](https://documentation.huma-num.fr/sharedocs-traitement/#reconnaissance-de-caracteres-ocr)

**ATTENTION :** Ce dossier, comme l'ensemble des sous-dossiers de **hnTools_watchFolder** servant aux traitements, ne sont pas des dossiers de sauvegarde. **L'ensemble des fichiers présents seront supprimés au bout d'un certain délai**. Veillez donc bien à sauvegarder le résultat du traitement sur votre espace personnel ou sur un espace partagé.

## III - Extraction du texte d'un PDF

Un autre cas pouvant se présenter est celui d'un document PDF contenant déjà une "sous-couche" avec une version textuelle du document. La présence de cette couche textuelle peut être rapidement vérifié par la sélection d'une partie du texte dans un lecteur PDF (*figure X)*.

| ![](img/xpdf_exemple.png)                                                                           |
|:---------------------------------------------------------------------------------------------------:|
| *Figure X - Capture d'écran d'un PDF ne nécessitant pas d'océrisation car contenant déjà le texte.* |

Dans ce cas, le PDF **ne doit pas être océrisé**. Il suffit **d'extraire la couche textuelle** du document. Pour ce faire, depuis votre espace ShareDocs, déposez votre fichier PDF dans le dossier **hnTools_watchFolder/PDF/xpdf/toTexte**. Vous serez notifié par mail lorsque l'extraction sera terminée. Une fois l'extraction terminée, un fichier *.txt* apparaitra au même emplacement. 

[Documentation Huma-Num de l'outil xpdf](https://documentation.huma-num.fr/sharedocs-traitement/#xpdf-conversion-txt)

**ATTENTION :** Ce dossier, comme l'ensemble des sous-dossiers de **hnTools_watchFolder** servant aux traitements, ne sont pas des dossiers de sauvegarde. **L'ensemble des fichiers présents seront supprimés au bout d'un certain délai**. Veillez donc bien à sauvegarder le résultat du traitement sur votre espace personnel ou sur un espace partagé. 

## IV - Liens utiles

- [ShareDocs - Outils de traitement - Documentation de l'IR* Huma-Num](https://documentation.huma-num.fr/sharedocs-traitement/)

- [Best Practices - Optical Character Recognition (OCR) @ Pitt - Guides at University of Pittsburgh](https://pitt.libguides.com/ocr/bestpractices)
